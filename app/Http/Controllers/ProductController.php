<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Entity\Product;
use App\Http\Requests\AddProductHttpRequest;
use App\Http\Requests\UpdateProductHttpRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;

final class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = Product::all();
        return view('products.index')->with('products', $products);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(AddProductHttpRequest $request)
    {
        $product = Product::create([
            'name' => $request->input('name'),
            'price' => (float)$request->input('price'),
            'user_id' => Auth::id()
        ]);

        return redirect()->route('products.index');
    }

    public function show(Product $product)
    {
        return view('products.show')->with('product', $product);
    }

    public function edit(Product $product)
    {
        try {
            $this->authorize('update', $product);
        } catch (AuthorizationException $e) {
            return redirect()->back();
        }
        return view('products.edit')->with('product', $product);
    }

    public function update(UpdateProductHttpRequest $request, Product $product)
    {
        try {
            $this->authorize('update', $product);
        } catch (AuthorizationException $e) {
            return redirect()->back();
        }
        $product->name = $request->input('name');
        $product->price = (float)$request->input('price');
        $product->save();

        return redirect()->route('products.show', ['id' => $product->id()]);
    }

    public function destroy(Product $product)
    {
        try {
            $this->authorize('delete', $product);
        } catch (AuthorizationException $e) {
            return redirect()->back();
        }
        $product->delete();
        return redirect()->route('products.index');
    }
}
