<?php

namespace App\Http\Controllers\Auth;

use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class GithubLoginController extends Controller
{
    protected $redirectTo = '/products';

    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleProviderCallback()
    {
        try {
            $githubUser = Socialite::driver('github')->user();
        } catch (\Exception $e) {
            return redirect()->back();
        }

        $user = User::where('email', $githubUser->email)->first();
        if($user){
            Auth::login($user, true);
        } else {
            $user = User::create([
                'name' => $githubUser->name,
                'email' => $githubUser->email,
                'password' => Hash::make(Str::random(8))
            ]);
            Auth::login($user, true);
        }

        return redirect($this->redirectTo);
    }
}
