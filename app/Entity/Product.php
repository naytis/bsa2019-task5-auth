<?php

declare(strict_types=1);

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Product
 * @package App\Entity
 * @property int $id
 * @property string $name
 * @property int $price
 * @property int $user_id
 */
final class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'name',
        'price',
        'user_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function price(): string
    {
        return number_format($this->price, 2, '.', '');
    }

    public function userId(): int
    {
        return $this->user_id;
    }
}
