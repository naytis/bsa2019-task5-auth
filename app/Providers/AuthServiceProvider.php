<?php

namespace App\Providers;

use App\Entity\Product;
use App\Entity\User;
use App\Policies\ProductPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Product::class => ProductPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-product', function (User $user, Product $product) {
            return $product->userId() === $user->id() || $user->isAdmin();
        });

        Gate::define('delete-product', function (User $user, Product $product) {
            return $product->userId() === $user->id() || $user->isAdmin();
        });
    }
}
