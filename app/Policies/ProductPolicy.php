<?php

namespace App\Policies;

use App\Entity\User;
use App\Entity\Product;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class ProductPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Product $product)
    {
        if (Gate::allows('update-product', $product)) {
            return true;
        }
    }

    public function delete(User $user, Product $product)
    {
        if (Gate::allows('delete-product', $product)) {
            return true;
        }
    }
}
