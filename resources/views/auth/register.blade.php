@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <h2 class="h2 mb-3">{{ __('Register') }}</h2>
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="form-group">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="{{ __('Name') }}">

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('E-Mail Address') }}">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" minlength="6" placeholder="{{ __('Password') }}">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" minlength="6" placeholder="{{ __('Confirm Password') }}">
                </div>

                <div class="form-group mb-4">
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary w-100">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
                <div class="justify-content-center">
                    <div class="middle-line">
                        <span>Or sign in with</span>
                    </div>
                </div>
                <div class="form-group mb-0 d-flex justify-content-center">
                    <div class="col-md-9 d-flex justify-content-around">
                        <a href="{{ route('redirectToGoogle') }}">
                            <i class="fab fa-google fa-2x"></i>
                        </a>
                        <a href="{{ route('redirectToFacebook') }}">
                            <i class="fab fa-facebook-f fa-2x"></i>
                        </a>
                        <a href="{{ route('redirectToGithub') }}">
                            <i class="fab fa-github fa-2x"></i>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
