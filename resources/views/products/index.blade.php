@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-3">
                <h1 class="h2">Products</h1>
                <a href="{{ route('products.create') }}" class="btn btn-success">Add</a>
            </div>
        </div>
        <div class="col-md-8">
            <div class="table-responsive">
                <table class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                    <tr id="row-{{ $product->id() }}">
                        <td>{{ $product->id() }}</td>
                        <td>
                            <span class="value-cell">
                                <a href="{{ route('products.show', $product->id()) }}">{{ $product->name() }}</a>
                            </span>
                        </td>
                        <td>
                            <span class="value-cell">
                                ${{ $product->price() }}
                            </span>
                        </td>

                        <td>
                            <div class="btn-group">
                                @can('update-product', $product)
                                    <a
                                        class="btn btn-primary btn-sm"
                                        href="{{ route('products.edit', $product->id()) }}"
                                    >
                                        Edit
                                    </a>
                                @endcan
                                @can('delete-product', $product)
                                    <form
                                        action="{{ route('products.destroy', $product->id()) }}"
                                        method="post"
                                        id="delete-form-{{ $product->id() }}"
                                    >
                                        @csrf
                                        @method('DELETE')

                                    </form>
                                    <button type="submit" class="btn btn-danger btn-sm" form="delete-form-{{ $product->id() }}">
                                        Delete
                                    </button>
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
