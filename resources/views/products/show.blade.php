@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="card col-4 p-0">
                <div class="card-body">
                    <h3 class="h3">Product #{{ $product->id() }}</h3>
                    <div class="row">
                        <div class="col-4">
                            <h4 class="h4">Name: </h4>
                        </div>
                        <div class="col-6">
                            <h4 class="h4">{{ $product->name() }}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <h4 class="h4">Price:</h4>
                        </div>
                        <div class="col-6">
                            <h4 class="h4">${{ $product->price() }}</h4>
                        </div>
                    </div>

                    <div class="d-flex flex-row justify-content-end">
                        @can('update-product', $product)
                            <a
                                class="btn btn-primary mx-1"
                                href="{{ route('products.edit', $product->id()) }}"
                            >
                                Edit
                            </a>
                        @endcan
                        @can('delete-product', $product)
                            <form
                                action="{{ route('products.destroy', $product->id()) }}"
                                method="post"
                                id="delete-form-{{ $product->id() }}"
                            >
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger mx-1">
                                    Delete
                                </button>
                            </form>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection