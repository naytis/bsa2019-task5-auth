@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-5">
                <h3 class="card-title h3">Update Product</h3>
                <form action="{{ route('products.update', $product->id()) }}" method="post" id="add-form">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <input class="form-control" name="name" type="text" placeholder="Name" value="{{ $product->name() }}" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="price" type="number" placeholder="Price" step="0.01" value="{{ $product->price() }}" required>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection