<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('/products', 'ProductController');
Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {
    Route::get('/google/redirect', 'GoogleLoginController@redirectToProvider')
        ->name('redirectToGoogle');
    Route::get('/google/callback', 'GoogleLoginController@handleProviderCallback');
    Route::get('/facebook/redirect', 'FacebookLoginController@redirectToProvider')
        ->name('redirectToFacebook');
    Route::get('/facebook/callback', 'FacebookLoginController@handleProviderCallback');
    Route::get('/github/redirect', 'GithubLoginController@redirectToProvider')
        ->name('redirectToGithub');
    Route::get('/github/callback', 'GithubLoginController@handleProviderCallback');
});